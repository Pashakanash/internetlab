#pragma once
#ifndef INTERNET_SUBSCRIPTION_H
#define INTERNET_SUBSCRIPTION_H

#include "constants.h"

struct date
{
    int hour;
    int minute;
    int second;
};

struct memory
{
    int memory;
    
};

struct internet_subscription 
{
    date start;
    date finish;
    memory send;
    memory get;
    char title[MAX_STRING_SIZE];
};

#endif
