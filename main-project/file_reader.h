#pragma once
#ifndef FILE_READER_H
#define FILE_READER_H

#include "internet_subscription.h"

void read(const char* file_name, internet_subscription* array[], int& size);

#endif