#include <iostream>
#include <iomanip>

#include <Windows.h>
using namespace std;

#include "internet_subscription.h"
#include "file_reader.h"
#include "constants.h"
#include <string>
void insertionSort11(internet_subscription** arr, int size) {
    int n = size;
    for (int i = 1; i < n; i++) {
        internet_subscription* key1 = arr[i];
        int key = arr[i]->finish.hour * 3600 + arr[i]->finish.minute * 60 + arr[i]->finish.second - arr[i]->start.hour * 3600 - arr[i]->start.minute * 60 - arr[i]->start.second;
        int j = i - 1;

        while (j >= 0 && (arr[j]->finish.hour * 3600 + arr[j]->finish.minute * 60 + arr[j]->finish.second - arr[j]->start.hour * 3600 - arr[j]->start.minute * 60 - arr[j]->start.second) < key) {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = key1;
    }
}

void insertionSort12(internet_subscription** arr, int size) {
    int n = size;
    for (int i = 1; i < n; i++) {
        internet_subscription* key1 = arr[i];
        int key = strlen(arr[i]->title);
        int j = i - 1;

        while (j >= 0 && strlen(arr[j]->title) > key) {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = key1;
    }
}

int partition1(internet_subscription** arr, int low, int high) {
    int pivot = arr[high]->finish.hour * 3600 + arr[high]->finish.minute * 60 + arr[high]->finish.second - arr[high]->start.hour * 3600 - arr[high]->start.minute * 60 - arr[high]->start.second;
    int i = low - 1;

    for (int j = low; j <= high - 1; j++) {
        if (arr[j]->finish.hour * 3600 + arr[j]->finish.minute * 60 + arr[j]->finish.second - arr[j]->start.hour * 3600 - arr[j]->start.minute * 60 - arr[j]->start.second > pivot) {
            i++;
            swap(arr[i], arr[j]);
        }
    }
    swap(arr[i + 1], arr[high]);
    return i + 1;
}

void quickSort1(internet_subscription** arr, int low, int high) {
    if (low < high) {
        int pi = partition1(arr, low, high);

        quickSort1(arr, low, pi - 1);
        quickSort1(arr, pi + 1, high);
    }
}

int partition2(internet_subscription** arr, int low, int high) {
    int pivot = strlen(arr[high]->title);
    int i = low - 1;

    for (int j = low; j <= high - 1; j++) {
        if (strlen(arr[j]->title) < pivot) {
            i++;
            swap(arr[i], arr[j]);
        }
    }
    swap(arr[i + 1], arr[high]);
    return i + 1;
}

void quickSort2(internet_subscription** arr, int low, int high) {
    if (low < high) {
        int pi = partition2(arr, low, high);

        quickSort2(arr, low, pi - 1);
        quickSort2(arr, pi + 1, high);
    }
}








int main()
{
    SetConsoleOutputCP(65001); // Устанавливаем кодировку UTF-8    cout << "Лабораторная работа №4. GIT\n";
    cout << "Вариант №5. Протокол работы в Интернет\n";
    cout << "Автор: Канашевич Павел\n\n";
    internet_subscription* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    cout << endl;
    cout << "===================================================";
    cout << endl;
    try
    {
        read("data.txt", subscriptions, size);
        cout << "Введите номер сортировки, который хотите применить: ";
        int n;
        int n1;
        cin >> n;

        switch (n)
        {
        case 1:
            cout << "Введите тип сортировки, который хотите применить: ";

            cin >> n1;
            switch (n1) {
            case 1:
                insertionSort11(subscriptions, size);
                break;
            case 2:
                insertionSort12(subscriptions, size);
                break;
            }
            break;
        case 2:
            cout << "Введите тип сортировки, который хотите применить: ";

            cin >> n1;
            switch (n1) {
            case 1:
                quickSort1(subscriptions, 0, size - 1);
                break;
            case 2:
                quickSort2(subscriptions, 0, size - 1);
                break;
            }
            break;
            break;
        default:
            break;
        }
        cout << endl;
        cout << "===================================================";
        cout << endl;
        cout << "***** Протокол работы в Интернет *****\n\n";
        for (int i = 0; i < size; i++)
        {

            /********** вывод начала времени использования сети Интернет **********/
            cout << "Время начала использования сети Интернет: ";
            // вывод часа время начала использования сети Интернет
            cout << subscriptions[i]->start.hour << ":";
            // вывод минуты время начала использования сети Интернет
            cout << subscriptions[i]->start.minute << ":";
            // вывод секунды время начала использования сети Интернет
            cout << subscriptions[i]->start.second;
            cout << '\n';
            /********** вывод конца времени использования сети Интернет **********/
            cout << "Время конца использования сети Интернет: ";
            // вывод часа время конца использования сети Интернет
            cout << subscriptions[i]->finish.hour << ":";
            // вывод минуты время конца использования сети Интернет
            cout << subscriptions[i]->finish.minute << ":";
            // вывод секунды время конца использования сети Интернет
            cout << subscriptions[i]->finish.second;
            cout << '\n';
            /********** вывод кол-ва байт  **********/
            // вывод получено
            cout << "Байт получено.....: ";
            cout << subscriptions[i]->send.memory;
            cout << '\n';
            // вывод отправлено
            cout << "Байт отправлено...: ";
            cout << subscriptions[i]->get.memory;
            cout << '\n';
            /********** вывод названия приложения выхода в интернет  **********/
            cout << "Полный путь к программе, которая осуществляла подключение к сети Интернет...: ";
            cout << subscriptions[i]->title;
            cout << '\n';
        }
        cout << endl;
        cout << "===================================================";
        cout << endl;
        cout << endl;
        cout << "Введите номер способа фильтрации данных: ";
        int n2;
        cin >> n2;
        cout << endl;
        cout << "===================================================";
        cout << endl;
        switch (n2)
        {
        case 1:
            for (int i = 0; i < size; i++)
            {
                if (strcmp(subscriptions[i]->title, "skype") == 0 || strcmp(subscriptions[i]->title, "Skype") == 0) {
                    /********** вывод начала времени использования сети Интернет **********/
                    cout << "Время начала использования сети Интернет: ";
                    // вывод часа время начала использования сети Интернет
                    cout << subscriptions[i]->start.hour << ":";
                    // вывод минуты время начала использования сети Интернет
                    cout << subscriptions[i]->start.minute << ":";
                    // вывод секунды время начала использования сети Интернет
                    cout << subscriptions[i]->start.second;
                    cout << '\n';
                    /********** вывод конца времени использования сети Интернет **********/
                    cout << "Время конца использования сети Интернет: ";
                    // вывод часа время конца использования сети Интернет
                    cout << subscriptions[i]->finish.hour << ":";
                    // вывод минуты время конца использования сети Интернет
                    cout << subscriptions[i]->finish.minute << ":";
                    // вывод секунды время конца использования сети Интернет
                    cout << subscriptions[i]->finish.second;
                    cout << '\n';
                    /********** вывод кол-ва байт  **********/
                    // вывод получено
                    cout << "Байт получено.....: ";
                    cout << subscriptions[i]->send.memory;
                    cout << '\n';
                    // вывод отправлено
                    cout << "Байт отправлено...: ";
                    cout << subscriptions[i]->get.memory;
                    cout << '\n';
                    /********** вывод названия приложения выхода в интернет  **********/
                    cout << "Полный путь к программе, которая осуществляла подключение к сети Интернет...: ";
                    cout << subscriptions[i]->title;
                    cout << '\n';
                }
            }
            break;
        case 2:
            for (int i = 0; i < size; i++)
            {
                if (subscriptions[i]->start.hour >= 8) {
                    /********** вывод начала времени использования сети Интернет **********/
                    cout << "Время начала использования сети Интернет: ";
                    // вывод часа время начала использования сети Интернет
                    cout << subscriptions[i]->start.hour << ":";
                    // вывод минуты время начала использования сети Интернет
                    cout << subscriptions[i]->start.minute << ":";
                    // вывод секунды время начала использования сети Интернет
                    cout << subscriptions[i]->start.second;
                    cout << '\n';
                    /********** вывод конца времени использования сети Интернет **********/
                    cout << "Время конца использования сети Интернет: ";
                    // вывод часа время конца использования сети Интернет
                    cout << subscriptions[i]->finish.hour << ":";
                    // вывод минуты время конца использования сети Интернет
                    cout << subscriptions[i]->finish.minute << ":";
                    // вывод секунды время конца использования сети Интернет
                    cout << subscriptions[i]->finish.second;
                    cout << '\n';
                    /********** вывод кол-ва байт  **********/
                    // вывод получено
                    cout << "Байт получено.....: ";
                    cout << subscriptions[i]->send.memory;
                    cout << '\n';
                    // вывод отправлено
                    cout << "Байт отправлено...: ";
                    cout << subscriptions[i]->get.memory;
                    cout << '\n';
                    /********** вывод названия приложения выхода в интернет  **********/
                    cout << "Полный путь к программе, которая осуществляла подключение к сети Интернет...: ";
                    cout << subscriptions[i]->title;
                    cout << '\n';
                }
            }
            break;
        default:
            break;
        }
<<<<<<< Updated upstream
        cout << endl;
        cout << "===================================================";
        cout << endl;
=======
        cout << "������� �������� ����������: ";
        char na[MAX_STRING_SIZE];
        cin.read(na, 1);
        char name[MAX_STRING_SIZE];
        cin.getline(name, MAX_STRING_SIZE);
        int time = process(subscriptions, size, name);
        cout << "time: " << time << endl;
        cout << name;
>>>>>>> Stashed changes
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}